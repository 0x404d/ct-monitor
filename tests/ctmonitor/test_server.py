import unittest
import unittest.mock

from ctmonitor.server import (
    Entry,
    LogServer,
    get_logservers,
    get_known_logservers,
    get_chrome_logservers,
)


class EntryTest(unittest.TestCase):

    def test_init_sets_values(self):
        """Entry initializer sets values"""
        entry = Entry({"leaf_input": "abcd", "extra_data": "efgh"})

        assert entry.leaf == "abcd"
        assert entry.extra == "efgh"

    def test_hash_eq(self):
        """Entry with equal values hash equal"""
        entry1 = Entry({"leaf_input": "abcd", "extra_data": "efgh"})
        entry2 = Entry({"leaf_input": "abcd", "extra_data": "efgh"})

        assert hash(entry1) == hash(entry2)

    def test_hash_ne(self):
        """Entry with unequal values hash unequal"""
        entry1 = Entry({"leaf_input": "abcd", "extra_data": "efgh"})
        entry2 = Entry({"leaf_input": "efgh", "extra_data": "abcd"})

        assert hash(entry1) != hash(entry2)

    def test_eq(self):
        """Entry with unequal values compare unequal"""
        entry1 = Entry({"leaf_input": "abcd", "extra_data": "efgh"})
        entry2 = Entry({"leaf_input": "abcd", "extra_data": "efgh"})

        assert entry1 == entry2

    def test_ne(self):
        """Entry with unequal values compare unequal"""
        entry1 = Entry({"leaf_input": "abcd", "extra_data": "efgh"})
        entry2 = Entry({"leaf_input": "efgh", "extra_data": "abcd"})

        assert entry1 != entry2

    def test_set_duplicates(self):
        """Entry won't make duplicates in sets"""

        counts = {
            ("abcd", "efgh"): 0,
            ("efgh", "abcd"): 0,
            ("aaaa", "aaaa"): 0,
        }

        testset = set()
        testset.add(Entry({"leaf_input": "abcd", "extra_data": "efgh"}))
        testset.add(Entry({"leaf_input": "abcd", "extra_data": "efgh"}))
        testset.add(Entry({"leaf_input": "abcd", "extra_data": "efgh"}))
        testset.add(Entry({"leaf_input": "abcd", "extra_data": "efgh"}))
        testset.add(Entry({"leaf_input": "efgh", "extra_data": "abcd"}))
        testset.add(Entry({"leaf_input": "efgh", "extra_data": "abcd"}))
        testset.add(Entry({"leaf_input": "efgh", "extra_data": "abcd"}))
        testset.add(Entry({"leaf_input": "efgh", "extra_data": "abcd"}))

        for entry in testset:
            counts[(entry.leaf, entry.extra)] += 1

        assert counts == {
            ("abcd", "efgh"): 1,
            ("efgh", "abcd"): 1,
            ("aaaa", "aaaa"): 0,
        }


class LogServerTest(unittest.TestCase):

    def setUp(self):
        """Prepare a log server instance for use in testing"""
        self.logserver = LogServer("http://mock")

    @unittest.mock.patch("urllib.request.urlopen")
    def test_call(self, patch):
        """Log Server API calls goes to the correct endpoint and parses JSON"""
        # Patch urlopen so that we can change its reutrn value and check it's
        # been called, without launching a testing HTTP server.
        patch.return_value = unittest.mock.Mock()
        patch.return_value.read.return_value = b'{"ez": "pz"}'

        # Perform the API call
        val = self.logserver.call("method")

        # Verify that we got the URL we expected
        patch.assert_called_with("http://mock/ct/v1/method")
        # Check that the value we got returned was a JSON deserialized version
        # of our "HTTP response"
        assert val == {"ez": "pz"}

        # Test get parameters
        patch.mock_reset()
        self.logserver.call("method", foo="bar")
        patch.assert_called_with("http://mock/ct/v1/method?foo=bar")

    @unittest.mock.patch("ctmonitor.server.LogServer.call")
    def test_get_sth(self, patch):
        """LogServer get_sth performs an API call and stores the result"""
        # Patch the API call method
        patch.return_value = {
            "tree_size": unittest.mock.Mock(),
            "timestamp": 1234,
        }

        # Call the method
        val = self.logserver.get_sth()

        # Check that the API call was performed
        patch.assert_called_with("get-sth")
        # Check returned and stored values
        assert val is patch.return_value
        assert self.logserver.sth is patch.return_value
        assert self.logserver

    @unittest.mock.patch("ctmonitor.server.LogServer.call")
    def test_get_entries(self, patch):
        """LogServer get_entries performs an API call and returns the results"""
        # Prepare a mock response
        patch.return_value = {
            "entries": [
                {"leaf_input": "ab", "extra_data": "ba"},
                {"leaf_input": "cd", "extra_data": "ef"},
            ]
        }

        # Call the method
        val = set(self.logserver.get_entries(0, 12))
        # Check that the aPI call was performed as expected
        patch.assert_called_with("get-entries", start=0, end=12)
        # Check that the return value is a set of extra_data
        assert val == {
            Entry({"leaf_input": "ab", "extra_data": "ba"}),
            Entry({"leaf_input": "cd", "extra_data": "ef"}),
        }

    def new_entries_sth_mock(self):
        """Mock callable for get_sth that will update the tree size"""
        self.tree_size = int(10)

    @unittest.mock.patch("ctmonitor.server.LogServer.get_sth",
                         new=new_entries_sth_mock)
    @unittest.mock.patch("ctmonitor.server.LogServer.get_entries")
    def test_get_new_entries(self, patch_entries):
        """LogServer get_new_entries refreshes STH and fetches new entries"""
        # We're only gonna mock two entries here as we'll cover some of the
        # other stuff through asserts on the patched mocks
        patch_entries.return_value = set({"bbbbbb", "dddddd", "cccccc"})
        returned_entries = len(patch_entries.return_value)

        # Set the old tree size and get new entries
        self.logserver.tree_size = 0
        val = set(list(self.logserver.get_new_entries()))
        assert val == {"bbbbbb", "dddddd", "cccccc"}

        # Assert calls to get_entries
        patch_entries.assert_has_calls([
            unittest.mock.call(0, 10 - 1),  # End = newest STH's tree_size
            unittest.mock.call(returned_entries, (returned_entries * 2) - 1),
            unittest.mock.call((returned_entries * 2),
                               (returned_entries * 3) - 1),
            # The last call should end at the newet STH's tree_size, which is
            # set to 10 by our mock function
            unittest.mock.call((returned_entries * 3), 10 - 1),
        ])


mock_serverlist = """
{
    "logs": [
        {"url": "https://test-url1"},
        {"url": "http://test-url2"},
        {"url": "test-url3"}
    ]
}
"""


@unittest.mock.patch("urllib.request.urlopen")
def test_get_logservers(patch):
    test_url = "test"

    # Change the urlopen patch so that it returns a mock response
    patch(test_url).read().decode.return_value = mock_serverlist
    patch.reset_mock()

    # Call the function
    ret = get_logservers(test_url)

    # Verify urllib call
    patch.assert_called_with(test_url)

    # Verify return values
    assert ret[0].url == "https://test-url1"
    assert ret[1].url == "http://test-url2"
    assert ret[2].url == "https://test-url3"


@unittest.mock.patch("ctmonitor.server.get_logservers")
def test_get_known_logservers(patch):
    """get_known_logservers calls get_logservers correctly and returns"""
    url = "https://www.gstatic.com/ct/log_list/all_logs_list.json"
    # Call the function
    ret = get_known_logservers()

    # Assert URL and return value
    patch.assert_called_with(url)
    patch_ret = patch(url)
    assert ret == patch_ret


@unittest.mock.patch("ctmonitor.server.get_logservers")
def test_get_chrome_logservers(patch):
    """get_known_logservers calls get_logservers correctly and returns"""
    url = "https://www.gstatic.com/ct/log_list/log_list.json"
    # Call the function
    ret = get_chrome_logservers()

    # Assert URL and return value
    patch.assert_called_with(url)
    patch_ret = patch(url)
    assert ret == patch_ret
