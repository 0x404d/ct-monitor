import base64
import struct
import unittest
import unittest.mock

from nose import SkipTest
from nose.tools import raises
from cryptography.hazmat.backends import default_backend

from ctmonitor import parse
from ctmonitor.server import Entry


# Skip flags
read_certificate_works = False


@unittest.mock.patch("cryptography.x509.load_der_x509_certificate")
def test_Certificate(patch):
    certMock = unittest.mock.Mock()
    chainMock = unittest.mock.Mock()

    cert = parse.Certificate([certMock, chainMock, chainMock])

    backend = default_backend()
    patch.assert_has_calls([
        unittest.mock.call(certMock, backend),
        unittest.mock.call(chainMock, backend),
        unittest.mock.call(chainMock, backend),
    ])

    assert cert.cert == patch(certMock, backend)
    assert cert.chain == [patch(chainMock, backend)] * 2


def test_read_merkletreeleaf():
    entry = b"Some entry data"
    # Version, leaftype, timestamp, entrytype
    data_in = (1, 2, 0x0102030405060708, 0xF1F2)
    formatstr = ">bbQH"
    # Mix it together
    bindata = struct.pack(formatstr, *data_in) + entry

    # Call and check the return values
    out = parse.read_merkletreeleaf(bindata)
    assert out["version"] == 1
    assert out["leaftype"] == 2
    assert out["timestamp"] == 0x0102030405060708
    assert out["entrytype"] == 0xF1F2
    assert out["entry"] == entry


@unittest.mock.patch("ctmonitor.parse.read_merkletreeleaf")
@raises(Exception)
def test_read_entry_unknown(patch):
    patch.return_value = {
        "leaftype": -1,
    }

    parse.read_entry({
        "leaf_input": "",
        "extra_data": "",
    })


@unittest.mock.patch("ctmonitor.parse.read_merkletreeleaf")
@unittest.mock.patch("ctmonitor.parse.read_x509")
@unittest.mock.patch("ctmonitor.parse.Certificate")
def test_read_entry_x509(patch_cert, patch_x509, patch_mtl):
    patch_mtl.return_value = {
        "leaftype": 0,
        "entrytype": 0,
    }

    mock_extra = str(unittest.mock.Mock()).encode("ascii")
    parse.read_entry(Entry({
        "leaf_input": "",
        "extra_data": base64.b64encode(mock_extra),
    }))

    patch_x509.assert_called_with(patch_mtl.return_value, mock_extra)

    patch_cert.assert_called_with(patch_x509())


@unittest.mock.patch("ctmonitor.parse.read_merkletreeleaf")
@unittest.mock.patch("ctmonitor.parse.read_precert")
@unittest.mock.patch("ctmonitor.parse.Certificate")
def test_read_entry_precert(patch_cert, patch_precert, patch_mtl):
    patch_mtl.return_value = {
        "leaftype": 0,
        "entrytype": 1,
    }

    mock_extra = str(unittest.mock.Mock()).encode("ascii")
    parse.read_entry(Entry({
        "leaf_input": "",
        "extra_data": base64.b64encode(mock_extra),
    }))

    patch_precert.assert_called_with(mock_extra)

    patch_cert.assert_called_with(patch_precert())


def test_read_certificate():
    global read_certificate_works
    certificate_data = b"\x00\x00\x06abcdefNOTCERTDATA"

    (cert, seek) = parse.read_certificate(certificate_data)

    assert cert == b"abcdef"
    assert seek == 3 + len(cert)

    read_certificate_works = True


def test_read_x509():
    # We're not gonna mock read_certificate as it'll be kinda janky to do
    # properly.
    if not read_certificate_works:
        raise SkipTest

    chain = parse.read_x509(
        {"entry": b"\x00\x00\x03ABC"},
        b"\x00\x00\x0d\x00\x00\x03DEF\x00\x00\x041234",
    )

    assert chain == [b"ABC", b"DEF", b"1234"]


def test_read_precert():
    # We're not gonna mock read_certificate as it'll be kinda janky to do
    # properly.
    if not read_certificate_works:
        raise SkipTest

    chain = parse.read_precert(
        b"\x00\x00\x03ABC\x00\x00\x0d\x00\x00\x03DEF\x00\x00\x041234",
    )

    print(chain)
    assert chain == [b"ABC", b"DEF", b"1234"]
