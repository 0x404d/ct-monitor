import unittest.mock

from nose import SkipTest

import ctmonitor.filter


is_match_works = False


def test_get_dn_strings():
    """ctmonitor.filter.get_dn_strings extracts DN components"""

    dn = [
        unittest.mock.Mock(),
        unittest.mock.Mock(),
        unittest.mock.Mock(),
    ]

    # Single use component
    dn[0].oid.dotted_string = "2.5.4.18"  # Post office box
    dn[0].value = "example_po_string"

    # Repeated components
    dn[1].oid.dotted_string = "2.5.4.11"  # OU
    dn[1].value = "unit 1"

    dn[2].oid.dotted_string = "2.5.4.11"  # OU
    dn[2].value = "unit 2"

    components = ctmonitor.filter.get_dn_strings(dn)

    assert components == {
        'POST_OFFICE_BOX': ['example_po_string'],
        'ORGANIZATIONAL_UNIT_NAME': ['unit 1', 'unit 2']
    }


def test_get_altnames():
    # Preapre mocks
    cert = unittest.mock.Mock()
    ext = unittest.mock.Mock()

    # We want to make the get_extension... method return a specific mock
    cert.extensions.get_extension_for_oid.return_value = ext
    # We need to make get_values_for_type return a list so that the set cast
    # doesn't fail.
    ext.value.get_values_for_type.return_value = ["some_test_name"]

    matches = ctmonitor.filter.get_altnames(cert)

    # Check expected calls
    oid_altname = ctmonitor.filter.ExtensionOID.SUBJECT_ALTERNATIVE_NAME
    cert.extensions.get_extension_for_oid.assert_called_with(oid_altname)
    ext.value.get_values_for_type.assert_called_with(ctmonitor.filter.DNSName)

    assert matches == {"some_test_name"}


def test_is_match():
    """ctmonitor.filter.is_match verifies domains properly"""
    global is_match_works

    domains = ["*.aaaaa.test", "?.aaaaa.test", "aaaaa.test", "aaaaaaaaa.test",
               "bbbbb.fail", "b.aaaaa.test"]
    filter = ["aaaaa.test", "bb.fail"]

    valid_domains = set()

    for domain in domains:
        for filter_domain in filter:
            if ctmonitor.filter.is_match(domain, filter_domain):
                valid_domains.add(domain)

    print(valid_domains)
    assert valid_domains == {"*.aaaaa.test", "?.aaaaa.test", "aaaaa.test",
                             "b.aaaaa.test"}
    is_match_works = True


def test_is_wildcard_match():
    if not is_match_works:
        raise SkipTest

    cases = [
        (True, "*", "no"),
        (True, "*.no", "domene.no"),
        (True, "*.domene.no", "sub.domene.no"),
        (False, "*.no", "sub.domene.no"),
        # Not wildcards
        (False, "no", "no"),
        (False, "test.no", "test.no"),
        (False, "test.com", "test.no"),
        # Redacted certs should be shortened to the same length as the match
        # domain then work as a substring match
        (True, "?", "no"),
        (True, "?.?.?", "sub.domene.no"),
        (True, "?.?.no", "sub.domene.no"),
        (True, "?.domene.no", "sub.domene.no"),
        (True, "?.?.?.no", "domene.no"),
        (False, "?.no", "sub.domene.no"),
        (False, "?.aa", "domene.no"),
        (False, "?.?.?.aa", "domene.no"),
    ]

    print("Should match, Cert domain, Match domain")
    for case in cases:
        print(case)
        assert ctmonitor.filter.is_wildcard_match(case[1], case[2]) == case[0]


@unittest.mock.patch("ctmonitor.filter.get_altnames")
def test_get_matching_altnames(patch):
    """ctmonitor.test_matching_altnames gets altnames and verifies them"""
    if not is_match_works:
        raise SkipTest

    filter_doms = ("aaaa.a", )
    patch.return_value = ["aaaa.a", "aa.a", "a.aaaa.a"]
    cert = unittest.mock.Mock()

    matching_domains = ctmonitor.filter.get_matching_altnames(cert, filter_doms)

    patch.assert_called_with(cert)

    print(matching_domains)
    assert matching_domains == {"aaaa.a", "a.aaaa.a"}


@unittest.mock.patch("ctmonitor.filter.get_matching_altnames")
@unittest.mock.patch("ctmonitor.filter.get_dn_strings")
@unittest.mock.patch("ctmonitor.filter.is_match")
@unittest.mock.patch("ctmonitor.filter.is_wildcard_match")
def test_filter_certs(wildcard_match, is_match, get_dn, get_altnames):
    """ctmonitor.test_filter_certs verifies CN and calls other functions"""
    if not is_match_works:
        raise SkipTest

    group = unittest.mock.Mock()
    group.domains = ["aaaa.a", "bbbb.b"]

    certificate = unittest.mock.Mock()
    certificate.data = {}

    get_dn.return_value = {
        "COMMON_NAME": ["aaaa.a"],
    }

    # We want the CN check to succeed, so we'll force this to True now. The rest
    # of the checks are patched out above
    get_altnames.return_value = set()
    is_match.reset_mock()
    is_match.return_value = True
    wildcard_match.reset_mock()
    wildcard_match.return_value = False

    out = ctmonitor.filter.filter_certs(None, {certificate}, group)
    assert out == {certificate}

    get_dn.assert_called_with(certificate.cert.subject)
    get_altnames.assert_called_with(certificate.cert, tuple(group.domains))

    # Verify that the arguments for the CN check is correct
    is_match.assert_has_calls([
        unittest.mock.call("aaaa.a", domain) for domain in group.domains
    ])
    wildcard_match.assert_not_called()

    # Do a run where we force the CN check to fail
    get_altnames.return_value = set()
    is_match.reset_mock()
    is_match.return_value = False
    wildcard_match.reset_mock()
    wildcard_match.return_value = False

    out = ctmonitor.filter.filter_certs(None, {certificate}, group)
    assert not out
    wildcard_match.assert_has_calls([
        unittest.mock.call("aaaa.a", domain) for domain in group.domains
    ])

    # Do a run where we force wildcard matches
    get_altnames.return_value = set()
    is_match.reset_mock()
    is_match.return_value = False
    wildcard_match.reset_mock()
    wildcard_match.return_value = True

    out = ctmonitor.filter.filter_certs(None, {certificate}, group)
    assert out == {certificate}
    wildcard_match.assert_has_calls([
        unittest.mock.call("aaaa.a", domain) for domain in group.domains
    ])
