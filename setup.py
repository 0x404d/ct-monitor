from setuptools import setup, find_packages

setup(
    name="ctmonitor",
    version="0.0.0",
    description="Certificate Transparency log monitor",
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=[
        "cryptography",
    ],
)
