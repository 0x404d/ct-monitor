import binascii
import json
import sys

from ctmonitor import filter


class NotificationGroup(object):
    """Groups a set of domain-names together with a name.

    This allows us to model entities such as customers.
    """

    def __init__(self, name: str, domains: set):
        """Constructor"""
        self.name = name
        self.domains = domains

    def test_write(self):
        print(self.name)
        print(self.domains)


class INotifier(object):
    """Generic interface for notifiers"""

    def __init__(self, config: "ctmonitor.config.Configuration"):
        self.config = config

    def add_group_matches(self, certs: {"ctmonitor.parse.Certificate"},
                          group: "ctmonitor.interface.NotificationGroup"):
        """Called after a set of certs gets past the filter process and matches
        a notification group
        """

        pass

    def finalize(self):
        """Called once the entire log update process is complete and all certs
        have been processed
        """

        pass


def get_fingerprint(cert: "cryptography.x509.Certificate"):
    fp = cert.fingerprint(cert.signature_hash_algorithm)
    return binascii.hexlify(fp).decode("ascii")


def get_json(certificate: "ctmonitor.parse.Certificate"):
    return {
        "fp": get_fingerprint(certificate.cert),
        "subject": filter.get_dn_strings(certificate.cert.subject),
        "issuer": filter.get_dn_strings(certificate.cert.issuer),
        "matches": list(certificate.data['matches']),
        "altnames": list(filter.get_altnames(certificate.cert)),
    }


class JSONBufferedReport(INotifier):
    """Buffered JSON report

    Will only make the report once all certs have been parsed and the program
    finishes.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.results = dict()

    def add_group_matches(self, certs: "ctmonitor.parse.Certificate",
                          group: "ctmonitor.interface.NotificationGroup"):
        """Store matched certs aside for later processing"""

        if group not in self.results:
            self.results[group] = set()

        self.results[group] |= certs

    def finalize(self):
        """Make the final report and write it to STDOUT"""
        output = dict()

        # Make one sub-dict for each notification group
        for group in self.results:
            group_results = dict()

            # Add all observed certs for this group to the output dict
            for certificate in self.results[group]:
                data = get_json(certificate)
                group_results.append(data)

            output[group.name] = group_results

        json.dump(output, sys.stdout, indent=4)


class JSONFeedReport(INotifier):
    """Feed-like JSON report

    Each match will be printed to the terminal as soon as possible. Note that
    there is a real chance of cross-logserver duplicates as we don't keep track
    of notified certs to minimize memory usage for larger runs.
    """

    def add_group_matches(self, certs, group):
        """Write out a new line with all info related to this cert"""

        for certificate in certs:
            out = get_json(certificate)
            out['notification_group'] = group.name

            print(json.dumps(out))
