import json
import logging
import os

from ctmonitor import parse, config


logging.basicConfig(filename="monitor.log", level=logging.DEBUG)
logger = logging.getLogger(__name__)

monitor_config = config.Configuration()

progress_file = "log_treesizes.json"


# Load parse state
progress = {}
if os.path.exists(progress_file):
    # Verify that it's actually a file
    if not os.path.isfile(progress_file):
        raise Exception("'&s' is not a file; unable to store log parsing "
                        "progress." % progress_file)

    with open(progress_file) as fh:
        progress = json.load(fh)


# Get entries
def get_entries_from_servers():
    for logserver in monitor_config.logservers:
        if logserver.url in monitor_config.server_blacklist:
            continue

        try:
            if logserver.url in progress:
                logserver.tree_size = progress[logserver.url]
                for entry in logserver.get_new_entries():
                    yield entry

            else:
                logserver.get_sth()

        except Exception as e:
            logger.exception(e)
            logger.error("Failed communicating with server %r", logserver)


results = dict()

for entry in get_entries_from_servers():
    cert = parse.read_entry(entry)

    # Filter certs
    for group in monitor_config.groups:
        if group not in results:
            results[group] = set()

        curr_results = set([cert])

        for filter in monitor_config.filters:
            curr_results = filter(monitor_config, curr_results, group)

        results[group] |= curr_results

        if curr_results:
            # Update notifiers with the new result
            for notifier in monitor_config.notifiers:
                notifier.add_group_matches(curr_results, group)

# Finalize notifiers
for notifier in monitor_config.notifiers:
    notifier.finalize()


# Save parse state
for logserver in monitor_config.logservers:
    progress[logserver.url] = logserver.parsed_tree_size


# Load parse state
with open(progress_file, "w") as fh:
    json.dump(progress, fh)
