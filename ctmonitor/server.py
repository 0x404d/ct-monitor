"""Implements utilities for communicating with CT log servers"""
import json
import logging
import urllib
import urllib.request
from urllib import parse


logger = logging.getLogger(__name__)


class Entry(object):
    """A hashable entry object

    This object allows us to store parsed entries in a set, in order to avoid
    duplicates.
    """

    def __init__(self, entry):
        self.leaf = entry["leaf_input"]
        self.extra = entry["extra_data"]

    def __hash__(self):
        return hash(frozenset(("leaf:" + self.leaf, "extra:" + self.extra)))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __ne__(self, other):
        return hash(self) != hash(other)


class LogServer(object):
    """Allows for interacting with a CT log server"""

    def __init__(self, url):
        """Initializer for a logserver instance

        URL field should be the root at which the log server API can be found.
        That is, everything up to and excluding "ct/v1/..."
        """
        self.url = url

        # STH response data
        self.sth = None
        self.tree_size = None
        self.parsed_tree_size = None

    def __repr__(self):
        """Human readable representation of a LogServer instance"""
        return "<LogServer %r, tree_size = %r>" % (self.url, self.tree_size)

    def call(self, path, **args):
        """Perform a call against the log server's HTTP API"""
        # Prepare URL and perform the request
        url = parse.urljoin(self.url, "ct/v1/%s" % path)
        if args:
            url += "?%s" % parse.urlencode(args)
        resp = urllib.request.urlopen(url)
        # Decode and return the result
        return json.loads(resp.read().decode("ascii"))

    def get_sth(self):
        """Get the current STH of this log server

        Returns the current STH and stores it on LogServer.sth.
        """
        logger.debug("Requesting STH for %r", self)
        self.sth = self.call("get-sth")
        # Store some values we want for log crawling
        self.tree_size = self.sth["tree_size"]
        # Return the STH
        logger.debug("New STH: %s (tree size: %s)", self.sth["timestamp"],
                     self.tree_size)
        return self.sth

    def get_entries(self, start, end):
        """Get log entries between the start and end tree sizes, inclusive"""
        logger.debug("Requesting entries %s to %s for %r", start, end, self)
        result = self.call("get-entries", start=start, end=end)
        logger.debug("Got %d entries", len(result["entries"]))

        # Store the entire entry and return it. The certificate is stored in
        # different ways depending on what kind of log entry this is.
        entries = set()
        for entry in result["entries"]:
            entries.add(Entry(entry))

        return entries

    def get_new_entries(self):
        """Fetch all inserted log entries since last STH refresh"""
        logger.debug("Requesting new entries for %r", self)
        # Store the old tree size and update the STH
        self.parsed_tree_size = self.tree_size
        self.get_sth()

        # Break early if we didn't get an STH
        if self.tree_size is None:
            return iter([])

        # If we don't have an STH from before, it doesn't really matter where we
        # start
        if self.parsed_tree_size is None:
            self.parsed_tree_size = self.tree_size

        # Get the amount of new certs, used to track current progress
        certs_left = self.tree_size - self.parsed_tree_size
        # Pre-init the read count to the amount of certs left. This should make
        # the server return as many entries as it wants, then we'll use that
        # later to determine how many entries to request per iteration
        read_count = certs_left

        # Fetch new certificates
        while certs_left > 0:
            # Use the previous iteration's amount of read entries to determine
            # the end range for this request
            # TODO: Maybe just leave the `end` as new_tree_size constantly? If
            # the first request works on all servers, this shouldn't really
            # matter anyways so might as well.
            end = self.parsed_tree_size + read_count

            # Some logs will cap get-entries request ends to the tree size, so
            # we'll need to handle that
            if end >= self.tree_size:
                end = self.tree_size

            # Fetch entries and yield
            entries = self.get_entries(self.parsed_tree_size, end - 1)
            for entry in entries:
                yield entry

            # Update the read count with the amount of entries we got back
            read_count = len(entries)
            # Reduce the certs_left counter by the amount of read certs
            certs_left -= read_count
            # Update the start of the entry range in preparation for the next
            # iteration
            self.parsed_tree_size += read_count


def get_logservers(url) -> [LogServer]:
    """Get a list of LogServer instances based on an online list of servers

    The URL should be any URL containing a list formatted like Google's CT log
    server lists.
    """
    logger.debug("Requesting log server list %s", url)

    # Request the server list and deserialize it
    resp = urllib.request.urlopen(url)
    serverlist = json.loads(resp.read().decode("utf-8"))

    # Initialize a LogServer list based on the response
    servers = []
    for log in serverlist["logs"]:
        url = log["url"]

        # Default to the HTTPS protocol if no valid protocol is given
        if not url.startswith("http"):
            url = "https://" + url

        servers.append(LogServer(url))

    return servers


def get_known_logservers() -> [LogServer]:
    """Return LogServer instances for Google's list of known log servers"""

    url = "https://www.gstatic.com/ct/log_list/all_logs_list.json"
    return get_logservers(url)


def get_chrome_logservers() -> [LogServer]:
    """Return LogServer instances for Chrome-trusted log servers"""

    url = "https://www.gstatic.com/ct/log_list/log_list.json"
    return get_logservers(url)
