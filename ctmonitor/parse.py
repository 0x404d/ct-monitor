import base64
import struct

from cryptography.hazmat.backends import default_backend
from cryptography import x509


class Certificate(object):
    """A certificate from the log and its entire chain"""

    def __init__(self, chain):
        """Constructor"""
        # Throw all the certs into OpenSSL
        self._raw_chain = chain
        certs = [
            x509.load_der_x509_certificate(cert, default_backend())
            for cert in chain
        ]

        self.cert = certs[0]
        self.chain = certs[1:]

        self.data = {}


def read_merkletreeleaf(leaf: bytes) -> dict:
    """Read the merkle tree leaf structure from the log entry's leaf input"""
    formatstr = ">bbQH"
    length = struct.calcsize(formatstr)
    (version, leaftype, timestamp, entrytype) = struct.unpack(formatstr,
                                                              leaf[:length])

    return {
        "version": version,
        "leaftype": leaftype,
        "timestamp": timestamp,
        "entrytype": entrytype,
        "entry": leaf[length:],
    }


def read_entry(entry: dict) -> Certificate:
    """Read a leaf entry and return the parsed certificate chains for it"""
    leaf = base64.b64decode(entry.leaf)
    extra = base64.b64decode(entry.extra)
    mtl = read_merkletreeleaf(leaf)

    if mtl["leaftype"] != 0:
        raise Exception("Unknown leaf type %d" % mtl["leaftype"])

    if mtl["entrytype"] == 0:
        # print("X509")
        chain = read_x509(mtl, extra)
        # print(cert)

    elif mtl["entrytype"] == 1:
        # print("PreCert")
        chain = read_precert(extra)
        # print(cert)

    return Certificate(chain)


def read_certificate(data: bytes) -> (bytes, int):
    """Read a serialized certificate from the input data"""
    # Get the length
    length = int.from_bytes(data[:3], "big")
    # Get the certificate
    cert = data[3:length + 3]
    # Return the certificate and the amount of bytes read
    return (cert, length + 3)


def read_x509(mtl: dict, extra: bytes) -> [bytes]:
    """Read a X.509 log entry and return the associated certificate chain"""
    certs = []
    # Read the certificate
    (cert, _) = read_certificate(mtl["entry"])
    certs.append(cert)

    # Prepare a seek index for the extra_data field. We're skipping the first 3
    # bytes as this is the total bytelength of the entire chain. We'll just use
    # the per-cetificate length field when reading certificates.
    i = 3
    while i < len(extra):
        # Read cert
        (cert, seek) = read_certificate(extra[i:])
        certs.append(cert)
        # Seek
        i += seek

    return certs


def read_precert(extra: bytes) -> [bytes]:
    """Read a precert log entry and return the associated certificate chain

    Precertificate essentially work the same as X.509 entries, except the
    certificate itself is prepended to the certificate chain in the extra_data.

    So in other words:
    <Cert certificate> + <int24 chainlength> + <Cert chaincert1> + <Cert …2>...
    """
    certs = []

    # Read the certificate
    (cert, seek) = read_certificate(extra)
    certs.append(cert)

    i = seek + 3
    while i < len(extra):
        # Read cert
        (cert, seek) = read_certificate(extra[i:])
        certs.append(cert)
        # Seek
        i += seek

    return certs
