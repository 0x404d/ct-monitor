import importlib
import json

from ctmonitor import server
from ctmonitor.interface import NotificationGroup


class Configuration(object):
    def __init__(self):
        self.load()

    def load(self):
        self.load_config()
        self.load_notification_groups()

    def load_notification_groups(self):
        with open("notification_groups.json") as sg:
            notification_groups = json.load(sg)

        groups = []
        for i in notification_groups:
            groups.append(NotificationGroup(i, set(notification_groups[i])))

        self.groups = groups

    def load_config(self):
        # Load basic config
        with open("config.json") as fh:
            data = fh.read()

        # Strip lines starting with //
        lines = data.split("\n")
        out_lines = []

        for line in lines:
            if not line.strip().startswith("//"):
                out_lines.append(line)

        data = "\n".join(out_lines)
        self.config = json.loads(data)

        # Initialize other configuration attributes from the config data
        self.filters = self.get_filters()
        self.notifiers = self.get_notifiers()
        self.server_blacklist = self.get_server_blacklist()
        self.logservers = self.get_logservers()

    def load_attr_from_list(self, path_list):
        attrs = []
        for attr_path in path_list:
            # Get path
            path = attr_path.split(".")
            attr_name = path[-1]
            module_path = ".".join(path[:-1])

            # Import function
            module = importlib.import_module(module_path)
            attr = getattr(module, attr_name)

            attrs.append(attr)
        return attrs

    def get_filters(self):
        return self.load_attr_from_list(self.config["filters"])

    def get_notifiers(self):
        notifiers = []

        notif_classes = self.load_attr_from_list(self.config["notifiers"])
        for cls in notif_classes:
            notifiers.append(cls(self))

        return notifiers

    def get_server_blacklist(self):
        return self.config["servers"]["blacklist"]

    def get_logservers(self):
        if self.config["servers"]["use_list"] == "chrome":
            return server.get_chrome_logservers()

        if self.config["servers"]["use_list"] == "known":
            return server.get_known()

        if self.config["servers"]["use_list"] == "json":
            return [
                server.LogServer(uri)
                for uri in self.config["servers"]["serverlist"]
            ]
