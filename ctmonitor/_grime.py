"""Monkey-patches and other fixes for malformed certs that doesn't fit upstream"""
# flake8:noqa: E501
import logging

from cryptography.x509 import name


logger = logging.getLogger(__name__)


x509NameAttribute__init__ = name.NameAttribute.__init__


# FIXME: Remove this as soon as possible
def patched_name_init(self, oid, value, _type=name._SENTINEL):
    """Patched NameAttribute initializer that will silently ignore ValueErrors.

    Some malformed certs contains values that will trip validation errors
    related to country names. This initializer will log those errors and
    manually initialize the object, bypassing regular validation in the
    Cryptography module.

    All code following the log error message has been copied from the
    cryptography.x509.name module's NameAttribute.__init__ method.

    This function is licensed under the same license as the file
    cryptography/x509/name.py, which as of May 8th, 2018 is:

        This file is dual licensed under the terms of the Apache License, Version
        2.0, and the BSD License. See the LICENSE file in the root of this repository
        for complete details.

    This file can be accessed from the following link:

        https://github.com/pyca/cryptography/blob/10cabad73b4e0cc15463e43f9a94855c4db7f032/src/cryptography/x509/name.py

    Link to the relevant license file as of May 8th, 2018:

        https://github.com/pyca/cryptography/blob/10cabad73b4e0cc15463e43f9a94855c4db7f032/LICENSE
    """

    try:
        x509NameAttribute__init__(self, oid, value, _type=_type)

    except ValueError as e:
        logger.warning("Squashed ValueError from cryptography.x509."
                       "NameAttribute.__init__. This is likely a malformed "
                       "certificate. See the exception below for details. "
                       "Initializing the NameAttribute by hand.\n"
                       "OID: %r\n"
                       "Value: %r\n"
                       "Type: %r", oid, value, _type)
        logger.exception(e)

        # The appropriate ASN1 string type varies by OID and is defined across
        # multiple RFCs including 2459, 3280, and 5280. In general UTF8String
        # is preferred (2459), but 3280 and 5280 specify several OIDs with
        # alternate types. This means when we see the sentinel value we need
        # to look up whether the OID has a non-UTF8 type. If it does, set it
        # to that. Otherwise, UTF8!
        if _type == name._SENTINEL:
            _type = name._NAMEOID_DEFAULT_TYPE.get(oid,
                                                   name._ASN1Type.UTF8String)

        if not isinstance(_type, name._ASN1Type):
            raise TypeError("_type must be from the _ASN1Type enum")

        self._oid = oid
        self._value = value
        self._type = _type


name.NameAttribute.__init__ = patched_name_init
