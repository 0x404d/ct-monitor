import logging
import ctmonitor.interface

from cryptography.x509 import NameOID, ExtensionOID, DNSName, ExtensionNotFound

logger = logging.getLogger(__name__)


# Map OID strings to names
oid_map = dict()

# Add OIDs that aren't part of cryptography (right now at least)
oid_map["2.5.4.18"] = "POST_OFFICE_BOX"
oid_map["1.2.840.113549.1.9.2"] = "PKCS9_UNSTRUCTURED_NAME"

# Add OIDs from the cryptography module
for attr in [key for key in dir(NameOID) if not key.startswith("_")]:
    oid_map[getattr(NameOID, attr).dotted_string] = attr


def get_dn_strings(dn: "cryptography.x509.Name") -> dict:
    """Return a dict of all components from the supplied distinguished name"""

    components = {}

    for dn_component in dn:
        oid = dn_component.oid.dotted_string

        if oid in oid_map:
            comp_type = oid_map[oid]
        else:
            comp_type = "OID:%s" % oid

        if comp_type not in components:
            components[comp_type] = []

        components[comp_type].append(dn_component.value)

    return components


def get_altnames(cert: "cryptography.x509.Certificate") -> {str}:
    """Return a set of all altnames tied to this certificate"""
    try:
        altname_ext = cert.extensions.get_extension_for_oid(
            ExtensionOID.SUBJECT_ALTERNATIVE_NAME
        ).value
    except ExtensionNotFound:
        logger.debug("No SUBJECT_ALTERNATIVE_NAME extension on cert %r", cert)
        return set()

    return set(altname_ext.get_values_for_type(DNSName))


def is_match(name: str, match_domain: str):
    if name.endswith(match_domain):
        # Verify that this is either a direct match on the name, or
        # that it  is a subdomain (matches a.no and a.a.no, but not
        # aa.no)
        no_ending = name[:-len(match_domain)]

        # Check that this is an exact match (the domain without match
        # domain should be empty) or that it is a match on a subdomain
        # for the match domain (the last character should be a dot.
        if len(no_ending) == 0 or no_ending[-1] == ".":
            return True

    return False


def is_wildcard_match(name: str, match_domain: str):
    """Function to explicitly match same-level wildcards against the match domain.
    Sub-level wildcards should be matched by the is_match method.
    """
    # Check if this is a wildcard first
    if name[0] not in "*?":
        return False

    domain_parts = name.split(".")
    match_parts = match_domain.split(".")

    # Shorten redacted labels so that they have the same component length as the
    # match domain
    if len(domain_parts) > len(match_parts):
        # Shorten observed domain to the same length as the match domain
        diff = len(domain_parts) - len(match_parts)
        domain_parts = domain_parts[diff:]

        logger.debug("Shortened redacted domain %r to %r", name,
                     ".".join(domain_parts))
        name = ".".join(domain_parts)

    # Check that both domains are on the same level
    if len(match_parts) == len(domain_parts):
        # Set the first level component of the wildcard domain to be equal
        # to the match domain's first level component.
        for i in range(len(match_parts)):
            if domain_parts[i] in "*?":
                domain_parts[i] = match_parts[i]

        domain = ".".join(domain_parts)

        logger.debug("Performed wildcard replacement %r -> %r (match "
                     "domain: %r)", name, domain, match_domain)

        return is_match(domain, match_domain)

    return False


def get_matching_altnames(cert: "cryptography.x509.Certificate",
                          match_domains: tuple) -> set:
    """Return all alternative domain names in the cert that matches a name in
    the supplied set of names"""
    domains = set()

    for name in get_altnames(cert):
        # Check that the domain ends with a match domain
        for match_domain in match_domains:
            if is_match(name, match_domain):
                domains.add(name)

            elif is_wildcard_match(name, match_domain):
                domains.add(name)

    return domains


def filter_certs(config: "ctmonitor.config.Configuration", certs: set,
                 group: "ctmonitor.interface.NotificationGroup") -> list:
    """Filter list of Certificates, return list containing matches"""
    filter_domains = tuple(group.domains)

    filtered_list = set()
    for certificate in certs:
        cert = certificate.cert

        if "matches" not in certificate.data:
            certificate.data["matches"] = set()

        # Check whether any altnames on this cert mathces the search domains
        domain_matches = get_matching_altnames(cert, filter_domains)

        # Get DN components from the certificate subject
        components = get_dn_strings(cert.subject)
        # Verify that we got a CN field
        if "COMMON_NAME" in components:
            # Check whether the CN matches the domain
            for common_name in components["COMMON_NAME"]:
                for filter_domain in filter_domains:
                    if is_match(common_name, filter_domain):
                        # Add it to the list
                        domain_matches |= {common_name}

                    elif is_wildcard_match(common_name, filter_domain):
                        domain_matches |= {common_name}

        # If we got any matching domains, add the certificate to the output set
        # and update the on-cert set of domain matches
        if domain_matches:
            filtered_list.add(certificate)
            certificate.data['matches'] |= domain_matches

    return filtered_list


seen_fingerprints = dict()


def remove_duplicates(config: "ctmonitor.config.Configuration", certs: set,
                      group: "ctmonitor.interface.NotificationGroup") -> list:
    """Skip processing of certificates that have already been observed"""
    out = set()

    if group not in seen_fingerprints:
        seen_fingerprints[group] = set()

    for cert in certs:
        fp = ctmonitor.interface.get_fingerprint(cert.cert)

        if fp not in seen_fingerprints[group]:
            seen_fingerprints[group].add(fp)
            out.add(cert)

        else:
            logger.debug("Filtered certificate %r", fp)

    return out
