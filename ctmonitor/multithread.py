import contextlib
import json
import logging
import os
import queue
import threading
import time

from ctmonitor import parse, config


logging.basicConfig(filename="monitor.log", level=logging.DEBUG)
logger = logging.getLogger(__package__ + ".multithread")

progress_file = "log_treesizes.json"

results = None
logfetcher = None
certqueue = None
progress = {}


class ResultTracker(object):
    def __init__(self):
        self.results = {}
        self.locks = {}
        self.lock_lock = threading.Lock()

    @contextlib.contextmanager
    def group_lock(self, tag):
        self.lock_lock.acquire()

        if tag not in self.locks:
            self.locks[tag] = threading.Lock()

        self.locks[tag].acquire()
        self.lock_lock.release()

        yield

        self.locks[tag].release()

    def add(self, tag, items):
        # Acquire the lock for this tag
        with self.group_lock(tag):
            # Initialize the set if not already
            if tag not in self.results:
                self.results[tag] = set()

            self.results[tag] |= items


def get_progress():
    # Load parse state
    if os.path.exists(progress_file):
        # Verify that it's actually a file
        if not os.path.isfile(progress_file):
            raise Exception("'%s' is not a file; unable to store log parsing "
                            "progress." % progress_file)

        with open(progress_file) as fh:
            data = json.load(fh)

        return data

    return {}


# Get entries
def get_entries_from_servers():
    for logserver in monitor_config.logservers:
        if logserver.url in monitor_config.server_blacklist:
            continue

        try:
            if logserver.url in progress:
                logserver.tree_size = progress[logserver.url]
                for entry in logserver.get_new_entries():
                    yield entry

            else:
                logserver.get_sth()

        except Exception as e:
            logger.exception(e)
            logger.error("Failed communicating with server %r", logserver)


class FilterThread(threading.Thread):

    def run(self):
        while logfetcher.is_alive() or not certqueue.empty():
            try:
                cert = parse.read_entry(certqueue.get(timeout=5))
            except queue.Empty:
                logger.info("No products for %r", self)
                continue

            # Filter certs
            for group in monitor_config.groups:
                curr_results = set([cert])

                for filter in monitor_config.filters:
                    curr_results = filter(monitor_config, curr_results, group)

                results.add(group, curr_results)

                if curr_results:
                    # Update notifiers with the new result
                    for notifier in monitor_config.notifiers:
                        notifier.add_group_matches(curr_results, group)

            certqueue.task_done()

        logger.info("Exiting filter thread %r", self)


class LogProducerThread(threading.Thread):

    def run(self):
        """Fetch new entries from the log and add them to the queue"""
        for entry in get_entries_from_servers():
            certqueue.put(entry)

        logger.info("Entry producer finished")


class QueueStatus(threading.Thread):

    def run(self):
        while logfetcher.is_alive() or not certqueue.empty():
            logger.info("Queue size: %s", certqueue.qsize())
            time.sleep(2)


if __name__ == "__main__":
    monitor_config = config.Configuration()
    results = ResultTracker()
    certqueue = queue.Queue(maxsize=100000)

    # Load progress
    progress = get_progress()

    threads = []

    logfetcher = LogProducerThread()
    logfetcher.start()
    threads.append(logfetcher)

    queuestatus = QueueStatus()
    queuestatus.start()

    for i in range(7):
        thread = FilterThread()
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    # Finalize notifiers
    for notifier in monitor_config.notifiers:
        notifier.finalize()

    # Save parse state
    for logserver in monitor_config.logservers:
        progress[logserver.url] = logserver.tree_size

    with open(progress_file, "w") as fh:
        json.dump(progress, fh)

    queuestatus.join()
